﻿using Newgistics.Lib.ServiceHost.Helper;
using Microsoft.AspNetCore.Hosting;

namespace Jim.Redo.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = WebHostHelper.GetHost(typeof(Startup), args);
            webHost.Run();
        }
    }
}
