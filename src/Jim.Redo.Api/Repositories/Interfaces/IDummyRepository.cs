﻿using Newgistics.Lib.ServiceHost.Dto;
using System.Threading.Tasks;

namespace Jim.Redo.Api.Repositories.Interfaces
{
    public interface IDummyRepository
    {
        Task<BooleanResult> GetDummy(string id);
    }
}
